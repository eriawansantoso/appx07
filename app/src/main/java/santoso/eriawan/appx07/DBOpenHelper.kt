package santoso.eriawan.appx07

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper

class DBOpenHelper (context: Context): SQLiteOpenHelper(context,DB_Name, null, DB_Ver){
    companion object{
        val DB_Name = "coba"
        val DB_Ver = 1
    }

    override fun onCreate(db: SQLiteDatabase?) {
        val tMhs = "create table mhs(nim text primary key, nama text not null, prodi text not null)"

        db?.execSQL(tMhs)


    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
        TODO("Not yet implemented")
    }
}