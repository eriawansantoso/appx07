package santoso.eriawan.appx07


import android.app.AlertDialog
import android.content.DialogInterface
import android.content.Intent
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.*
import com.google.zxing.BarcodeFormat
import com.google.zxing.integration.android.IntentIntegrator
import com.journeyapps.barcodescanner.BarcodeEncoder
import kotlinx.android.synthetic.main.activity_main.*
import java.util.*


class MainActivity : AppCompatActivity() , View.OnClickListener {
    lateinit var intentIntegrator: IntentIntegrator
    lateinit var db : SQLiteDatabase
    lateinit var builder : AlertDialog.Builder
    lateinit var adapter : ListAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        intentIntegrator = IntentIntegrator(this)
        btnScanQr.setOnClickListener(this)
        btnGenerateQR.setOnClickListener(this)
        db = DBOpenHelper(this).writableDatabase
        btnSimpan.setOnClickListener(this)
        builder = AlertDialog.Builder(this)
        lsMhs.setOnItemClickListener(itemClick)
    }
    fun getDBObject() : SQLiteDatabase{
        return db
    }

    override fun onClick(v: View?) {

        when(v?.id){
            R.id.btnScanQr -> {
//                inisisi saat scan barcode , akan berbunyi beep saat terdeteksi
                intentIntegrator.setBeepEnabled(true).initiateScan()
            }
            R.id.btnGenerateQR -> {
                val barCodeEncorder = BarcodeEncoder()
                val bitmap = barCodeEncorder.encodeBitmap(edQrCode.text.toString(),
                    BarcodeFormat.QR_CODE, 400, 400)
                imV.setImageBitmap(bitmap)
            }
            R.id.btnSimpan ->{
                builder.setTitle("Konfirmasi").setMessage("Data yang akan dimasukkan sudah benar?")
                    .setIcon(android.R.drawable.ic_dialog_info)
                    .setPositiveButton("Ya",btnInsertDialog)
                    .setNegativeButton("Tidak",null)
                builder.show()
            }

        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        val intentResult  = IntentIntegrator.parseActivityResult(requestCode,resultCode,data)
        if(intentResult!=null){
            if(intentResult.contents != null){
                edQrCode.setText(intentResult.contents)
                val token = StringTokenizer(edQrCode.text.toString(),";",false)
                edNim.setText(token.nextToken())
                edNama.setText(token.nextToken())
                edProdi.setText(token.nextToken())
            }else{
                Toast.makeText(this, "Dibatalkan",Toast.LENGTH_SHORT).show()
            }
        }
        super.onActivityResult(requestCode, resultCode, data)
    }
    fun showDataMhs(){
        val cursor : Cursor = db.query("mhs", arrayOf("nim as _id", "nama", "prodi" ),
            null, null, null,null,"nama asc")
        adapter = SimpleCursorAdapter(this,R.layout.item_mhs,cursor,
            arrayOf("_id","nama","prodi"), intArrayOf(R.id.txnim, R.id.txnama, R.id.txprodi),
            CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER)
        lsMhs.adapter = adapter
    }
    override fun onStart() {
        super.onStart()
        showDataMhs()
    }
    val itemClick = AdapterView.OnItemClickListener{ parent, view, position, id ->
        val c: Cursor = parent.adapter.getItem(position) as Cursor
        edNim.setText(c.getString(c.getColumnIndex("_id")))
        edNama.setText(c.getString(c.getColumnIndex("nama")))
        edProdi.setText(c.getString(c.getColumnIndex("prodi")))
    }
    fun insertDataMhs(nim : String, nama : String , prodi : String){
        var sql = "insert into mhs (nim,nama,prodi) values (?,?,?)"
        db.execSQL(sql, arrayOf(nim,nama,prodi))
        showDataMhs()
    }
    val btnInsertDialog = DialogInterface.OnClickListener{ dialog, which ->
        insertDataMhs(edNim.text.toString(), edNama.text.toString(), edProdi.text.toString())
        edNim.setText("")
        edNama.setText("")
        edProdi.setText("")
    }


}
